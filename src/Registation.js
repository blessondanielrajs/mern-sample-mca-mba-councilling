import React, { Component } from "react";
import {
    Card, Col, Row, Input, Button, Modal, PageHeader, message
} from "antd";
import { } from '@ant-design/icons';
import "./App.less";
import axios from "axios";

import config from './config';
class App extends Component {
    state = {


        status: "",
        first_name: "",
        last_name: "",
        community: "",
        ph_no: "",
        email: "",
        appliction_number: "",
        tancet_registration_number: "",
        tancet_mark:"",
        password: "",
        degree: "",
        confirm_password: "",
        
    };



    onChangeInputBox1 = (e) => {
        this.setState({ first_name: e.target.value });

    };
    onChangeInputBox2 = (e) => {
        this.setState({ last_name: e.target.value });

    };
    onChangeInputBox3 = (e) => {
        this.setState({ community: e.target.value });

    };
    onChangeInputBox4 = (e) => {
        this.setState({ ph_no: e.target.value });

    };
    onChangeInputBox5 = (e) => {
        this.setState({ email: e.target.value });


    };
    onChangeInputBox6 = (e) => {
        this.setState({ appliction_number: e.target.value });

    };
    onChangeInputBox7 = (e) => {
        this.setState({ tancet_registration_number: e.target.value });

    };
    onChangeInputBox8 = (e) => {
        this.setState({ tancet_mark: e.target.value });

    };

    onChangeInputBox9 = (e) => {
        this.setState({ degree: e.target.value });

    };
    onChangeInputBox10 = (e) => {
        this.setState({ password: e.target.value });

    };
    onChangeInputBox11 = (e) => {
        this.setState({ confirm_password: e.target.value });

    };

    register = () => {
        let flag = 0;
        var pattern_phno = /^[6-9]\d{9}$/;
        var pattern_email = /\S+@\S+\.\S+/;
        var pattern_appno = /^[0-9]\d{9}$/;
        var pattern_tanregno = /^[0-9]\d{7}$/;
        var pattern_password = /^[A-Za-z]\w{6,14}$/;
        var pattern_tanmark = /^-?\d*(\.\d+)?$/;
        let first_name = this.state.first_name;
        let last_name = this.state.last_name;
        let community = this.state.community;
        let ph_no = this.state.ph_no;
        let email = this.state.email;
        let appliction_number = this.state.appliction_number;
        let tancet_registration_number = this.state.tancet_registration_number;
        let tancet_mark=this.state.tancet_mark;
        let password = this.state.password;
        let degree = this.state.degree;
        let confirm_password = this.state.confirm_password;

        if (!first_name === "" || !/^[A-Za-z ]+$/.test(first_name)) {
            message.error("Invaild Input Frist Name");
            flag = 1;
            return false;
        }
        else if (!last_name === "" || !/^[A-Za-z ]+$/.test(last_name)) {
            message.error("Invaild Input Last Name");
            flag = 1;
            return false;
        }
        else if (!community === "" || !/^[A-Za-z ]+$/.test(community)) {
            message.error("Invaild Input Community");
            flag = 1;
            return false;
        }

        else if (!ph_no === "" || !pattern_phno.test(ph_no)) {
            message.error("Invaild Input Phone Number");
            flag = 1;
            return false;
        }
        else if (!email === "" || !pattern_email.test(email)) {
            message.error("Invaild Input email");
            flag = 1;
            return false;
        }
        else if (!appliction_number === "" || !pattern_appno.test(appliction_number)) {
            message.error("Invaild Input Application Number");
            flag = 1;
            return false;
        }

        else if (!tancet_registration_number === "" || !pattern_tanregno.test(tancet_registration_number)) {
            message.error("Invaild Input Tancet Registration Number");
            flag = 1;
            return false;
        }
        else if (!tancet_mark === "" || !pattern_tanmark.test(tancet_mark)) {
            message.error("Invaild Input Tancet Mark");
            flag = 1;
            return false;
        }


        else if (!degree === "" || !/^[A-Za-z. ]+$/.test(degree)) {
            message.error("Invaild Input Degree");
            flag = 1;
            return false;
        }
        else if (!password === "" || !pattern_password.test(password)) {
            message.error("Invaild Input Password");
            flag = 1;
            return false;
        }
        else if (password !== confirm_password) {
            message.error("Mismatch Password");
            flag = 1;
            return false;
        }
        else if (flag == 0) {
            let data = {
                first_name: this.state.first_name.trim(),
                last_name: this.state.last_name.trim(),
                community: this.state.community.trim(),
                ph_no: this.state.ph_no.trim(),
                email: this.state.email.trim(),
                "appliction_number": this.state.appliction_number.trim(),
                "tancet_registration_number": this.state.tancet_registration_number.trim(),
                "password": this.state.password.trim(),
                "degree": this.state.degree.trim(),
                "tancet_mark":this.state.tancet_mark.trim(),

            }
            axios.post(config.serverurl + "/pg_db/admin/registation", data)
                .then(async res => {
                    if (res.data.status === 1) {
                        message.success("Successfully register");
                        this.setState({ status: 1 })
                    }
                    else {
                        message.error("!Operation Failed");
                    }


                })
        }






    }



    render() {


        return (
            <div>

                <Row gutter={[16, 16]} align="middle">
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="FirstName" onChange={this.onChangeInputBox1} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="LastName" onChange={this.onChangeInputBox2} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="Community" onChange={this.onChangeInputBox3} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="PhoneNumber" onChange={this.onChangeInputBox4} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="Email" onChange={this.onChangeInputBox5} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="ApplicationNumber" onChange={this.onChangeInputBox6} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="Tancet Registration Number" onChange={this.onChangeInputBox7} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="Tancet Mark" onChange={this.onChangeInputBox8} /></Col>

                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="Degree" onChange={this.onChangeInputBox9} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="Password" onChange={this.onChangeInputBox10} /></Col>
                    <Col span={12}><Input disabled={this.state.status === 1 ? true : false} placeholder="Confirm Password" onChange={this.onChangeInputBox11} /></Col>
                    <Col span={12} offset={7}><Button block type="primary" onClick={this.register   }>Submit</Button></Col>
                </Row>









            </div>
        );
    }
}


export default App;


