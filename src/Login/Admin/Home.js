import React, { Component } from "react";
import {
    Statistic, Card, Row, Col, Typography, Switch
} from "antd";
import { } from '@ant-design/icons';
import config from '../../config';
import axios from "axios";
const { Title, Text } = Typography;
const names = ["* 10th Mark Sheet", "* 12th Mark Sheet / Diploma Certificate", "Provisional / Degree Certificate (optional)", "Transfer Certificate (optional)", "* TANCET 2021 Mark sheet", "* TANCET 2021 Hall Ticket", "* Permanent Community Certificate", "* Passport size photo of Applicant", "* Signature of Applicant", "Semester 1 mark sheet", "Semester 2 mark sheet", "Semester 3 mark sheet", "Semester 4 mark sheet", "Semester 5 mark sheet", "Semester 6 mark sheet", "Additional certificate 1 (optional)", "Additional certificate 2 (optional)"];
let redcolor = { backgroundColor: "#ff000024" };
let greencolor = { backgroundColor: "#1da57a42" };

class App extends Component {
    state = {
        key: 0,
        count: {},
        checked: false

    };
    componentDidMount() {
        axios.post(config.serverurl + "/pg_db/admin/statics")
            .then(res => {
                this.setState({ count: res.data.status, key: 1, checked: res.data.checked })
            })
    }

    Switch = (checked) => {
        this.setState({ checked: checked })

        let data = {
            checked: checked
        }

        axios.post(config.serverurl + "/pg_db/admin/checkedlogin", data)
            .then(res => {
            })
    }

    render() {
        console.log(this.state.checked)
        return (
            <div>
                <Row gutter={[16, 16]}>
                    <Col span={18}>
                        <Title level={3}>Admin Dasboard</Title>

                    </Col>
                    <Col span={3}>
                        <Text strong>Single Registration</Text>
                    </Col>
                    <Col span={2}>
                        <Switch checkedChildren="Enable" unCheckedChildren="Disable" checked={this.state.checked} onChange={this.Switch} />
                    </Col>

                    <Col span={24}>
                        <Title level={5}>Verification Pending Status</Title>

                    </Col>
                    {
                        this.state.key === 1 ?
                            Object.keys(this.state.count).map((keyName, i) => (
                                <Col span={6} key={i}>
                                    <Card style={this.state.count[keyName] === 0 ? greencolor : redcolor}>
                                        <Statistic
                                            title={names[i]}
                                            value={this.state.count[keyName]}
                                            precision={0}
                                            valueStyle={{ color: '#3f8600' }}
                                        />
                                    </Card>
                                </Col>
                            ))
                            : ''
                    }
                </Row>
            </div>
        );
    }
}

export default App;