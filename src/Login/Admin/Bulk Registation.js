import React, { Component } from "react";
import {
    Row, Col, Card, Typography, Button, Upload, message, Tag, Space, Statistic, Table
} from "antd";
import { UploadOutlined, DownloadOutlined, InboxOutlined } from '@ant-design/icons';
import { CSVLink } from "react-csv";
import config from '../../config';
import Sample from '../../Documents/file.csv';
const { Title, Paragraph, Text } = Typography;
const { Dragger } = Upload;



class App extends Component {
    state = {
        status: 0,
        data: []
    };

    onFileChange = async (info) => {
        const { status } = info.file;
        if (status !== 'uploading') {

        }
        if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
            this.setState({ data: info.file.response.addStatus });
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    };


    render() {




        const props = {
            accept: ".csv",
            name: 'file',
            multiple: true,
            action: config.serverurl + '/pg_db/admin/bulk',
            onChange: this.onFileChange,
        };


        const columns = [
            {
                title: 'Type',
                dataIndex: 'json',
                key: 'key',

            },
            {
                title: 'Message',
                dataIndex: 'msg',
                key: 'key',
                render: (text, record) => <Tag color={record.addstatus === 0 ? "red" : "green"}>{text}</Tag>

            },


        ];


        const headers = [
            { label: "Type", key: "json" },
            { label: "Message", key: "msg" },

        ];


        return (
            <div>
                <Row gutter={[16, 16]}>
                    <Col span={24}>
                        <Title level={2}>Bulk Registration</Title>
                    </Col>
                    <Col span={24}>

                        <Button target="_blank" href={Sample}>Download Template</Button>

                    </Col>
                    <Col span={24}>
                        <Dragger{...props}>
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">
                                Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                                band files
                            </p>
                        </Dragger>
                    </Col>

                    <Col span={24}>
                        <br />
                        <br />
                        {
                            this.state.data.length ?

                                <CSVLink data={this.state.data} headers={headers} filename={"Bulk Status"}>
                                    <DownloadOutlined />Download Excel
                                </CSVLink>
                                :
                                ""
                        }
                    </Col>

                    <Col span={24}>
                        {this.state.data.length > 0 ? <Table dataSource={this.state.data} columns={columns} /> : ""}
                    </Col>
                </Row>
            </div>
        );
    }
}


export default App;