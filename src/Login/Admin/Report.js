import React, { Component } from "react";
import {
    Row, Col, Table, message, Input, Space, Button, Typography, Descriptions, Badge, Tag, Select, Empty
} from "antd";
import { DownloadOutlined } from '@ant-design/icons';
import { CSVLink } from "react-csv";
import axios from "axios";
import config from '../../config';

const { Title } = Typography;
const { Search } = Input;
const { Option } = Select;
const type = ["sslc", "hsc", "degree", "tc", "tn-marksheet", "tn-hallticket", "cc", "sign", "photo", "sem1", "sem2", "sem3", "sem4", "sem5", "sem6", "add-certificate1", "add-certificate2"];
const names = ["* 10th Mark Sheet", "* 12th Mark Sheet / Diploma Certificate", "Provisional / Degree Certificate (optional)", "Transfer Certificate (optional)", "* TANCET 2021 Mark sheet", "* TANCET 2021 Hall Ticket", "* Permanent Community Certificate", "* Passport size photo of Applicant", "* Signature of Applicant", "Semester 1 mark sheet", "Semester 2 mark sheet", "Semester 3 mark sheet", "Semester 4 mark sheet", "Semester 5 mark sheet", "Semester 6 mark sheet", "Additional certificate 1 (optional)", "Additional certificate 2 (optional)"];

class App extends Component {
    state = {
        status: "",
        topic: "",
        uploadstatus: "",
        studentdata: []

    };

    handleChangeSelectType = (value) => {
        this.setState({ topic: value })

    }

    handleChangeSelectStatus = (value) => {
        this.setState({ status: parseInt(value) })
    }

    submitbtn = () => {
        let data = {
            topic: this.state.topic,
            status: this.state.status,
        };
        axios.post(config.serverurl + "/pg_db/admin/searchreport", data)
            .then(res => {
                if (res.data.studentdata.length)
                    this.setState({ studentdata: res.data.studentdata })
                else
                    this.setState({ studentdata: [] })

            })
    }


    acceptbtn = (id) => {

        let data = {
            id: id,
            branch: this.state.topic,
        }
        axios.post(config.serverurl + "/pg_db/admin/accept", data)
            .then(res => {
                if (res.data.Status === 1) {
                    let studentdata = this.state.studentdata;
                    studentdata = studentdata.filter(function (obj) {
                        return obj._id !== id;
                    });
                    this.setState({ studentdata: studentdata });
                    message.info("Information Accepted");

                }
            })
    }

    rejectbtn = (id) => {
        let data = {
            id: id,
            branch: this.state.topic,
        }
        axios.post(config.serverurl + "/pg_db/admin/reject", data)
            .then(res => {
                if (res.data.Status === 1) {
                    let studentdata = this.state.studentdata;
                    studentdata = studentdata.filter(function (obj) {
                        return obj._id !== id;
                    });
                    this.setState({ studentdata: studentdata });
                    message.info("Resubmission Updated");
                }
            })
    }

    render() {
        const columns = [
            {
                title: 'User ID',
                dataIndex: '_id',
                key: '_id',
            },
            {
                title: 'Frist Name',
                dataIndex: 'first_name',
                key: 'first_name',
            },
            {
                title: 'Action',
                dataIndex: '_id',
                key: '_id',
                render: (text, record) => (
                    this.state.status !== -2 ?
                        <Space>
                            <Button target="_blank" href={config.serverurl + "/uploads/" + text + "/" + this.state.topic + ".pdf"}>VIEW</Button>
                            <Button disabled={this.state.status === 1 ? true : false} type="primary" onClick={this.acceptbtn.bind(this, text)}>ACCEPT</Button>
                            <Button disabled={this.state.status === -1 ? true : false} danger type="primary" onClick={this.rejectbtn.bind(this, text)}>RESUBMISSION</Button>
                        </Space> : <Tag>Empty</Tag>

                )
            },
        ];

        const headers = [
            { label: "User ID", key: "_id" },
            { label: "First Name", key: "first_name" },
            { label: "Last Name", key: "last_name" },
            { label: "Email ID", key: "email" },
            { label: "Degree", key: "degree" },
            { label: "Community", key: "community" },
            { label: "Applicatoin number Name", key: "appliction_number" },
            { label: "Phone No.", key: "ph_no" },
        ];

        return (
            <div>
                <Row gutter={[16, 16]}>
                    <Col span={24}>
                        <Title level={2}>Report Statistics</Title>

                    </Col>
                    <Col span={8}>
                        <Select placeholder="Choose Document" style={{ width: "100%" }} onChange={this.handleChangeSelectType}>
                            {
                                type.map(function (item, i) {
                                    return <Option key={i} value={item}>{names[i]}</Option>
                                })
                            }
                        </Select>
                    </Col>
                    <Col span={8}>
                        <Select placeholder="Choose Status" style={{ width: "100%" }} onChange={this.handleChangeSelectStatus}>
                            <Option value={-2}>Not Uploaded</Option>
                            <Option value={0}>Uploaded / Pending</Option>
                            <Option value={1}>Accepted</Option>
                            <Option value={-1}>Resubmission</Option>
                        </Select>
                    </Col>
                    <Col span={4}>
                        <Button block type="primary" onClick={this.submitbtn} disabled={this.state.topic && this.state.status !== '' ? false : true}>Submit</Button>
                    </Col>
                    <br />
                    <br />
                    <Col span={24}>
                        <Title level={5}>List Data</Title>

                    </Col>
                    {
                        this.state.studentdata.length ?
                            <Col span={24}>
                                <CSVLink data={this.state.studentdata} headers={headers} filename={"Report Statistics"}>
                                    <DownloadOutlined />Download Excel
                                </CSVLink>
                            </Col> :
                            ""}


                    {
                        this.state.studentdata.length ?

                            <Col span={24}>

                                <Table dataSource={this.state.studentdata} columns={columns} />

                            </Col> :
                            <Col span={24} align="middle"><Empty /></Col>
                    }
                </Row>
            </div>
        );
    }
}

export default App;