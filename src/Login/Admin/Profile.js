import React, { Component } from "react";
import {
    Row, Col, Table, message, Input, Space, Button, Typography, Descriptions, Badge, Tag
} from "antd";

import axios from "axios";
import config from '../../config';

const { Title } = Typography;
const { Search } = Input;


class App extends Component {
    state = {
        oldpassword: "",
        newpassword: "",
        confirmPassword: "",
    };


    onChangeInputBox1 = (e) => {
        this.setState({ oldpassword: e.target.value });

    };
    onChangeInputBox2 = (e) => {
        this.setState({ newpassword: e.target.value });

    };
    onChangeInputBox3 = (e) => {
        this.setState({ confirmPassword: e.target.value });

    };

    submit = () => {
        let flag = 0;
        let newpassword = this.state.newpassword;
        var pattern_password = /^[A-Za-z]\w{6,14}$/;
        if (!newpassword === "" || !pattern_password.test(newpassword))
        {
            message.error("Invaild Password");
            flag = 1;
            return false;
        }
        else if (newpassword !== this.state.confirmPassword)
        {
            message.error("Mismatch Password");
            return false;
        }
        else if(flag===0)
        {
            let data = {
                id: this.props.data._id.trim(),
                oldpassword: this.state.oldpassword.trim(),
                confirmPassword: this.state.confirmPassword.trim()
            }
            axios.post(config.serverurl + "/pg_db/admin/profile", data)
                .then(res => {
                    if (res.data.Status === 1) {
                        message.info("Successfull Updated !");
                    }
                    else if (res.data.Status === -1) {
                        message.error("Old Password  Wrong !");
                    }
                    else {
                        message.error("Operation Failed !");
                    }
                })
        }

        
    }

    render() {

        return (
            <div>
                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title level={2}>My Profile</Title>

                    </Col>
                    <Col span={6} >
                        <Input placeholder="Old Password" value={this.state.oldpassword} style={{ width: "100%" }} onChange={this.onChangeInputBox1} />

                    </Col>
                    <Col span={6}>
                        <Input placeholder="New Password" value={this.state.newpassword} style={{ width: "100%" }} onChange={this.onChangeInputBox2} />

                    </Col>
                    <Col span={6} >
                        <Input placeholder="confirm Password" value={this.state.confirmPassword} style={{ width: "100%" }} onChange={this.onChangeInputBox3} />
                    </Col>
                    <Col span={6}  >
                        <Button block type="primary" onClick={this.submit} style={{ width: "80%" }}>Submit</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default App;