import React, { Component } from "react";
import {
    Row, Col, Table, message, Input, Space, Button, Typography, Modal, Descriptions, Tag, Empty
} from "antd";
import { SearchOutlined, DownloadOutlined, Html5Outlined} from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import { CSVLink } from "react-csv";
import axios from "axios";
import config from '../../config';
const type = ["sslc", "hsc", "degree", "tc", "tn-marksheet", "tn-hallticket", "cc", "sign", "photo", "sem1", "sem2", "sem3", "sem4", "sem5", "sem6", "add-certificate1", "add-certificate2"];

const { Title } = Typography;
class App extends Component {
    state = {
        status: "",
        searchText: '',
        searchedColumn: '',
        studentdata: [],
        statusInfo: {},
        key: 0,
        studentid: 0
        , user_details: {}
    };


    componentDidMount() {

        let data = {
            type: "admin"
        };
        axios.post(config.serverurl + "/pg_db/admin/user", data)
            .then(res => {
              
                if (res.data.Status === 1) {
                    this.setState({ studentdata: res.data.user_details });
                }
                else {
                    message.error('No User');
                }

            })

    }


    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({ closeDropdown: false });
                            this.setState({
                                searchText: selectedKeys[0],
                                searchedColumn: dataIndex,
                            });
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    oncheck = async (temp) => {
        await this.setState({ studentid: parseInt(temp._id) });
     

        let data = {
            searchstudent: parseInt(temp._id),
            filenames: type
        };
        await axios.post(config.serverurl + "/pg_db/admin/searchstudent", data)
            .then(res => {
            
                if (res.data.Status === 1) {
                    this.setState({ statusInfo: res.data.statusInfo, user_details: res.data.user_details[0], key: 1 });
                }
                else {
                    this.setState({ key: 0 });
                    message.error('No User');
                }
            })
    }

    handleCancel = () => {
        this.setState({ key: 0 });
    };

    handleOk = () => {
        this.setState({ key: 0 });
    };

    acceptbtn = (type) => {
        let data = {
            id: this.state.studentid,
            branch: type
        }
        axios.post(config.serverurl + "/pg_db/admin/accept", data)
            .then(res => {
                if (res.data.Status === 1)
                    this.setState({ user_details: res.data.userdetail[0] })

                message.info("Information Updated");
            })
    }

    rejectbtn = (type) => {
        let data = {
            id: this.state.studentid,
            branch: type
        }
        axios.post(config.serverurl + "/pg_db/admin/reject", data)
            .then(res => {
                if (res.data.Status === 1)
                    this.setState({ user_details: res.data.userdetail[0] })
                message.info("Information Updated");
            })
    }


    statusColor = (a, type) => {
        let studentid = this.state.studentid;
        let user_details = this.state.user_details;
        if (a) return (
            <Space>
                <Button target="_blank" href={config.serverurl + "/uploads/" + studentid + "/" + type + ".pdf"}>VIEW</Button>
                <Button type="primary" onClick={this.acceptbtn.bind(this, type)}>ACCEPT</Button>
                <Button type="primary" onClick={this.rejectbtn.bind(this, type)} danger>RESUBMISSION</Button>
                {
                    user_details.status[type] === 1 ? <Tag color="green">Accepted</Tag> :
                        user_details.status[type] === -1 ? <Tag color="red">Resubmission</Tag>
                            : <Tag color="blue">Pending</Tag>
                }
            </Space>
        )
        else return <Tag color="red">Not Uploaded</Tag>;
    }



    render() {
        let { key } = this.state;

        const columns = [
            {
                title: 'User ID',
                dataIndex: '_id',
                key: '_id',
                width: '5%',
                sorter: (a, b) => a._id - b._id,
            },
            {
                title: 'Frist Name',
                dataIndex: 'first_name',
                key: '_id',
                width: '10%',
                ...this.getColumnSearchProps('first_name'),
            },
            {
                title: 'Last Name',
                dataIndex: 'last_name',
                key: '_id',
                ...this.getColumnSearchProps('last_name'),
            },
            {
                title: 'Phone Number',
                dataIndex: 'ph_no',
                key: '_id',
                ...this.getColumnSearchProps('ph_no'),
            },
            {
                title: 'Role',
                dataIndex: 'role',
                key: '_id',
                ...this.getColumnSearchProps('phone_number'),
            },

            {
                title: 'Email:',
                dataIndex: 'email',
                key: '_id',
                width: '10%',
                ...this.getColumnSearchProps('email'),
            },
            {
                title: 'Community',
                dataIndex: 'community',
                key: '_id',
                width: '5%',
                ...this.getColumnSearchProps('community'),
            },
            {
                title: 'Tancet Registration Number',
                dataIndex: 'tancet_registration_number',
                key: '_id',
                width: '10%',
                sorter: (a, b) => a.tancet_registration_number - b.tancet_registration_number,
                sortDirections: ['descend', 'ascend'],
            },
            {
                title: 'Appliction Number',
                dataIndex: 'appliction_number',
                key: '_id',
                width: '10%',
                sorter: (a, b) => a.appliction_number - b.appliction_number,
                sortDirections: ['descend', 'ascend'],
            },
            {
                title: 'Tancet Mark',
                dataIndex: 'tancet_mark',
                key: '_id',
                width: '10%',
                sorter: (a, b) => a.tancet_mark - b.tancet_mark,
                sortDirections: ['descend', 'ascend'],
            },
            


            {
                title: 'Degree',
                dataIndex: 'degree',
                key: '_id',
                ...this.getColumnSearchProps('degree'),
            },
            {
                title: 'Action',
                dataIndex: '_id',
                key: '_id',
                render: (text, record) => (<Space>
                    <Button type="primary" onClick={this.oncheck.bind(this, record)}><Html5Outlined />STATUS</Button>
                </Space>
                )
            },

        ];

        const headers = [
            { label: "User ID", key: "_id" },
            { label: "First Name", key: "first_name" },
            { label: "Last Name", key: "last_name" },
            { label: "Phone No.", key: "ph_no" },
            { label: "Role", key: "role" },
            { label: "Email ID", key: "email" },
            { label: "Community", key: "community" },
            { label: "Tancet Registration Number", key: "tancet_registration_number" },
            { label: "Applicatoin number Name", key: "appliction_number" },
            { label: "Tancet Mark", key: "tancet_mark" },
            { label: "Degree", key: "degree" },
            { label: "SSLC", key: "status.sslc" },
            { label: "HSC", key: "status.hsc" },
            { label: "tc", key: "status.tc" },
            { label: "tn-marksheet", key: "status.tn-marksheet" },
            { label: "tn-hallticket", key: "status.tn-hallticket" },
            { label: "cc", key: "status.cc" },
            { label: "sign", key: "status.sign" },
            { label: "photo", key: "status.photo" },
            { label: "sem1", key: "status.sem1" },
            { label: "sem2", key: "status.sem2" },
            { label: "sem3", key: "status.sem3" },
            { label: "sem4", key: "status.sem4" },
            { label: "sem5", key: "status.sem5" },
            { label: "sem6", key: "status.sem6" },
            { label: "add-certificate1", key: "status.add-certificate1" },
            { label: "add-certificate2", key: "status.add-certificate2" },
        ];

        return (
            <div>
                <Row gutter={[16, 16]}>
                    <Col span={24}>
                        <Title level={2}>User Information</Title>
                    </Col>
                    {
                        this.state.studentdata.length ?
                            <Col span={24}>
                                <Button type="primary">
                                <CSVLink data={this.state.studentdata} headers={headers} filename={"User Information"}>
                                    <DownloadOutlined />Download Excel
                                    </CSVLink></Button>
                            </Col> :
                            <Col span={24} align="middle"><Empty /></Col>}
                    
                    <Col span={24}>
                        {this.state.studentdata.length > 0 ? <Table dataSource={this.state.studentdata} columns={columns} size="small"/> : ""}
                    </Col>
                </Row>

                <Modal title="Status" visible={this.state.key} width={800} onCancel={this.handleCancel} onOk={this.handleOk} forceRender>
                    {key === 1 ?
                        <Descriptions title="Status" bordered column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}>
                            <Descriptions.Item label="10th Mark Sheet"> {this.statusColor(this.state.statusInfo.sslc, type[0])}  </Descriptions.Item>
                            <Descriptions.Item label="12th Mark Sheet / Diploma Certificate">  {this.statusColor(this.state.statusInfo.hsc, type[1])}  </Descriptions.Item>
                            <Descriptions.Item label="Provisional / Degree Certificate "> {this.statusColor(this.state.statusInfo.degree, type[2])}</Descriptions.Item>
                            <Descriptions.Item label="Transfer Certificate"> {this.statusColor(this.state.statusInfo.tc, type[3])} </Descriptions.Item>
                            <Descriptions.Item label="TANCET 2021 Mark sheet" >
                                {this.statusColor(this.state.statusInfo['tn-marksheet'], type[4])}
                            </Descriptions.Item>
                            <Descriptions.Item label="TANCET 2021 Hall Ticket" >
                                {this.statusColor(this.state.statusInfo['tn-hallticket'], type[5])}
                            </Descriptions.Item>
                            <Descriptions.Item label="Permanent Community Certificate"> {this.statusColor(this.state.statusInfo.cc, type[6])} </Descriptions.Item>
                            <Descriptions.Item label="Passport size photo of Applicant"> {this.statusColor(this.state.statusInfo.sign, type[7])} </Descriptions.Item>
                            <Descriptions.Item label="Signature of Applicant"> {this.statusColor(this.state.statusInfo.photo, type[8])}</Descriptions.Item>
                            <Descriptions.Item label="Semester 1 mark sheet"> {this.statusColor(this.state.statusInfo.sem1, type[9])}  </Descriptions.Item>
                            <Descriptions.Item label="Semester 2 mark sheet"> {this.statusColor(this.state.statusInfo.sem2, type[10])} </Descriptions.Item>
                            <Descriptions.Item label="Semester 3 mark sheet">  {this.statusColor(this.state.statusInfo.sem3, type[11])} </Descriptions.Item>
                            <Descriptions.Item label="Semester 4 mark sheet">   {this.statusColor(this.state.statusInfo.sem4, type[12])} </Descriptions.Item>
                            <Descriptions.Item label="Semester 5 mark sheet">   {this.statusColor(this.state.statusInfo.sem5, type[13])} </Descriptions.Item>
                            <Descriptions.Item label="Semester 6 mark sheet">   {this.statusColor(this.state.statusInfo.sem6, type[14])} </Descriptions.Item>
                            <Descriptions.Item label="Additional certificate 1">   {this.statusColor(this.state.statusInfo['add-certificate1'], type[15])} </Descriptions.Item>
                            <Descriptions.Item label="Additional certificate 2">   {this.statusColor(this.state.statusInfo['add-certificate2'], type[16])}</Descriptions.Item>
                        </Descriptions> : ''
                    }
                </Modal>

            </div>
        );
    }
}

export default App;