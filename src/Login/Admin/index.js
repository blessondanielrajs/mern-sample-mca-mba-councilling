import React, { Component } from "react";
import {
    Layout, Menu, Typography
} from "antd";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    CloudUploadOutlined,

     UnorderedListOutlined,
  SecurityScanOutlined
} from '@ant-design/icons';

import Home from './Home';
import UserList from './User_List';
import Report from './Report';
import Profile from './Profile';
import BulkRegistaion from './Bulk Registation';

const { Header, Sider, Content } = Layout;

const { Title } = Typography;

class App extends Component {
    state = {
        collapsed: false,
        status: 0,
        key: 0
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    Home = () => {
        this.setState({
            status: 0,
        });
    };

    User_List = () => {
        this.setState({
            status: 1,
        });
    };


    Report = () => {
        this.setState({
            status: 2,
        });
    };
    profile=()=>{
        this.setState({
            status: 3,
        });
    }
    BulkRegistaion = () => {
        this.setState({
            status: 4,
        });
    }

    render() {
        return (
            <>
                <Layout>
                    <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
                        <div className="logo" style={{ color: "white" }}> <Title style={{ color: "white" }} level={3}>Admin</Title> </div>
                        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                            <Menu.Item key="1" icon={<UserOutlined />} onClick={this.Home}>
                                Home
                            </Menu.Item>
                            <Menu.Item key="2" icon={<UnorderedListOutlined />} onClick={this.User_List}>
                                User List
                            </Menu.Item>
                            <Menu.Item key="3" icon={<SecurityScanOutlined />} onClick={this.Report}>
                                Report
                            </Menu.Item>
                            <Menu.Item key="4" icon={<SecurityScanOutlined />} onClick={this.profile}>
                                Profile
                            </Menu.Item>
                            <Menu.Item key="5" icon={<CloudUploadOutlined />} onClick={this.BulkRegistaion}>
                                BulkRegistaion
                            </Menu.Item>
                        </Menu>
                    </Sider>

                    <Layout className="site-layout">
                        <Header className="site-layout-background" style={{ padding: 0 }}>
                            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                                className: 'trigger',
                                onClick: this.toggle,
                            })}


                        </Header>
                        <Content
                            className="site-layout-background"
                            style={{
                                margin: "16px 16px",
                                padding: 24,
                                minHeight: 280,
                            }}
                        >
                            <div >

                                {this.state.status === 0 ? <div><Home data={this.props.data}/></div>
                                    : this.state.status === 1 ? (<div><UserList data={this.props.data}/></div>)
                                            : this.state.status === 2 ? (<div><Report data={this.props.data}/></div>)
                                                : this.state.status === 3 ? (<div><Profile data={this.props.data}/></div>)
                                                    : this.state.status === 4 ? (<div> <BulkRegistaion/></div>)
                                                :""
                                }

                            </div>

                        </Content>
                    </Layout>
                </Layout>

            </>


        );
    }
}


export default App;


