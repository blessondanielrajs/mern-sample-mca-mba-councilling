import React, { Component } from "react";
import {
    Row, Col, Card, Typography, Button, Upload, message, Tag, Space, Statistic
} from "antd";
import { UploadOutlined, DownloadOutlined } from '@ant-design/icons';
import config from '../../config';
const { Title, Paragraph, Text } = Typography;
const names = ["* 10th Mark Sheet", "* 12th Mark Sheet / Diploma Certificate", "Provisional / Degree Certificate (optional)", "Transfer Certificate (optional)", "* TANCET 2021 Mark sheet", "* TANCET 2021 Hall Ticket", "* Permanent Community Certificate", "* Passport size photo of Applicant", "* Signature of Applicant", "Semester 1 mark sheet", "Semester 2 mark sheet", "Semester 3 mark sheet", "Semester 4 mark sheet", "Semester 5 mark sheet", "Semester 6 mark sheet", "Additional certificate 1 (optional)", "Additional certificate 2 (optional)"];

const info = ["Supported formats are PNG, JPEG, JPG, JPE & PDF, File size 150KB to 1MB", "Supported formats are PNG, JPEG, JPG, JPE & PDF, File size 150KB to 1MB", "Supported formats are PNG, JPEG, JPG, JPE & PDF, File size 150KB to 1MB", "Supported formats are PNG, JPEG, JPG, JPE & PDF, File size 150KB to 1MB", "Supported formats are PNG, JPEG, JPG, JPE & PDF, File size 150KB to 1MB", "Supported formats are PNG, JPEG, JPG, JPE & PDF, File size 150KB to 1MB", "Supported formats are PNG, JPEG, JPG, JPE & PDF, File size 150KB to 1MB", "Supported formats are PNG,JPEG,JPG & JPE, Fix size 20KB to 50KB", "Supported formats are PNG,JPEG,JPG & JPE, Fix size 20KB to 50KB", "Supported formats are PNG,JPEG,JPG,JPE & PDF, max size 1MB, both sides of mark sheet should be uploaded as single file", "Supported formats are PNG,JPEG,JPG,JPE & PDF, max size 1MB, both sides of mark sheet should be uploaded as single file", "Supported formats are PNG,JPEG,JPG,JPE & PDF, max size 1MB, both sides of mark sheet should be uploaded as single file", "Supported formats are PNG,JPEG,JPG,JPE & PDF, max size 1MB, both sides of mark sheet should be uploaded as single file", "Supported formats are PNG,JPEG,JPG,JPE & PDF, max size 1MB, both sides of mark sheet should be uploaded as single file", "Supported formats are PNG,JPEG,JPG,JPE & PDF, max size 1MB, both sides of mark sheet should be uploaded as single file", "Supported formats are PNG,JPEG,JPG,JPE & PDF, File size 150KB to 1MB", "Supported formats are PNG,JPEG,JPG,JPE & PDF, File size 150KB to 1MB"
];

const type = ["sslc", "hsc", "degree", "tc", "tn-marksheet", "tn-hallticket", "cc", "photo", "sign", "sem1", "sem2", "sem3", "sem4", "sem5", "sem6", "add-certificate1", "add-certificate2"];

class App extends Component {
    state = {
        status: 1,
        buttons: [],
        data: {},
        uploaded:0,
        notupload:0,
        resubmisssion:0,
        accept:0
    };

    async componentDidMount() {
        await this.setState({ data: this.props.data });
        await this.add();
        await this.calculatestatus();
    }

    onFileChange = async (info) => {
        if (info.file.status !== 'uploading') {
        }
        if (info.file.status === 'done') {
            await this.setState({ data: info.file.response.userdetail[0] });
            await this.add();
            message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    }

    calculatestatus=()=>
    {
        let data=this.state.data.status;
        let uploaded=0;
        let accept=0;
        let resubmisssion=0;
        let notupload=0;
        for (const key of Object.keys(data)) {
            
            if(data[key]===0)
            {
                uploaded++;
            }
            else if(data[key]===1)
            {
                accept++;
            }
            else if(data[key]===-1)
            {
                resubmisssion++;
            }
            else
            {
                notupload++;
            }
        }
        
        this.setState({ uploaded: uploaded, accept: accept, resubmisssion: resubmisssion, notupload: notupload})
    }

    add = () => {
        const button = [];

        for (let i = 0; i <= 16; i++) {

            const props = {
                accept: ".pdf",
                name: 'file',
                multiple: false,
                action: config.serverurl + "/pg_db/student/home",
                data: {
                    _id: this.state.data._id,
                    filename: type[i],
                },
                headers: {
                    authorization: 'authorization-text',
                },
                onChange: this.onFileChange,
                beforeUpload: (file) => {
                    const PDF = file.type === 'application/pdf';
                    if (!PDF) {
                        message.error('You can only upload PDF file!');
                    }
                    const isLt1M = file.size / 1024 / 1024 < 1;
                    if (!isLt1M) {
                        message.error('File must smaller than 1MB!');
                    }
                    return PDF && isLt1M;
                },
            };

            button.push(
                <Col span={24} key={i}>
                    <Card title={names[i]} extra={
                        this.state.data.status[type[i]] === 1 ? <Tag color="green">Accepted</Tag>
                            : this.state.data.status[type[i]] === -1 ? <Tag color="red">Resubmission</Tag>
                                : this.state.data.status[type[i]] === 0 ? <Tag color="blue">Uploaded / Verification Pending</Tag>
                                    : <Tag>Not Uploaded</Tag>
                    }>
                        <Row gutter={[16, 16]}>
                            <Col span={24}>
                                <Row gutter={[16, 16]}>
                                    <Col span={4}>
                                        <Upload  {...props}>
                                            <Button icon={<UploadOutlined />}>Click to Upload</Button>
                                        </Upload>
                                    </Col>
                                    <Col span={20}>
                                        {
                                            <Space>
                                                {
                                                    this.state.data.status[type[i]] >= 0 ?
                                                        <Button type="primary" icon={<DownloadOutlined />} target="_blank" href={config.serverurl + "/uploads/" + this.state.data._id + "/" + type[i] + ".pdf"}>Download</Button>
                                                        : ''
                                                }
                                            </Space>
                                        }
                                    </Col>
                                    <Col span={24}>
                                        <Text disabled>{info[i]}</Text>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>

                </Col>);
        }
        this.setState({ buttons: button });
    }

    render() {
    
        return (
            <div>
                <Row gutter={[16, 16]}>
                    <Col span={24}>
                        <Card
                            title="UPLOAD CERTFICATES SECTION"

                            style={{ width: "100%" }}
                            bordered
                        >

                            <Col span={24}>

                                <Card
                                    style={{ width: "100%" }}
                                    bordered
                                >

                                    <Typography>
                                        <Title level={4}>Introduction</Title>
                                        <Paragraph>
                                            1. All file that are marked with astericks (*) symbol are mandatory, you must upload them. You are instructed to upload all semester mark sheets.
                                        </Paragraph>
                                        <Paragraph>
                                            2. Select a file and upload for each certificate separately, you can also drag and drop the file to the upload button to initiate upload.

                                        </Paragraph>
                                        <Paragraph>
                                            3. Each certificate can be uploaded in an image or PDF file format, file size should be from 150KB to 1MB. Supported image formats are JPG,JPEG,JPE & PNG.

                                        </Paragraph>
                                        <Paragraph>
                                            4. Passport size photo and Signature of the Applicant should be uploaded only in image format, file size should be from 20KB to 50KB. Supported image formats are JPG,JPEG,JPE & PNG.

                                        </Paragraph>
                                        <Paragraph>
                                            5. To replace new version of file, either delete the file using delete icon and upload new one or just upload new one which will replace the existing version.

                                        </Paragraph>
                                        <Paragraph>
                                            6. If there are any additional certificates to be produced , please upload them via additional certificate option.

                                        </Paragraph>
                                        <Paragraph>
                                            7. Both sides of semester mark sheet should be uploaded, make sure you scan or take picture of both pages and convert to single file before upload

                                        </Paragraph>
                                        <Paragraph>
                                            8. Once upload is completed, make sure you freeze the upload inorder to start your certificate verification from our end, but once uploads are frozen, you will not be able to delete or re-upload the certificate again, kindly double check before freeze.

                                        </Paragraph>
                                        <Paragraph>
                                            9. Certificates that are marked as optional, you can ignore them if you don't have or not applicable.

                                        </Paragraph>
                                        <Paragraph>

                                            10. If file upload fails, retry the upload again.
                                        </Paragraph>


                                    </Typography>
                                </Card>
                            </Col>
                        </Card>

                    </Col>


                    <Title level={4}>Certificates Status</Title>

<Col span={24}>
                        <Card title="Statistic"
                    >
                        <Row gutter={[16, 16]}>
                            <Col span={24}>
                                <Row gutter={[16, 16]}>
                                    <Col span={6}>
                                              <Statistic
                                                title="Accept"
                                                value={this.state.accept}
                                                precision={0}
                                                valueStyle={{ color: '#3f8600' }}
                                            />
                                    </Col>
                                    <Col span={6}>
                                            <Statistic
                                                title="Upload"
                                                value={this.state.uploaded}
                                                precision={0}
                                                valueStyle={{ color: '#3f8600' }}
                                            />
                                    </Col>
                                    <Col span={6}>
                                            <Statistic
                                                title="Resubmission"
                                                value={this.state.resubmisssion}
                                                precision={0}
                                                valueStyle={{ color: '#3f8600' }}
                                            />
                                    </Col>
                                        <Col span={6}>
                                            <Statistic
                                                title="Not Upload"
                                                value={this.state.notupload}
                                                precision={0}
                                                valueStyle={{ color: '#3f8600' }}
                                            />
                                        </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>

                    </Col>

                    {
                        this.state.buttons.map(function (item, i) {
                            return item;
                        })
                    }

                </Row>
            </div>
        );
    }
}


export default App;