import React, { Component } from "react";
import {
    Input, Row, Col, Button, message, Typography
} from "antd";
import { } from '@ant-design/icons';
import axios from "axios";
import config from '../../config';
const { Title } = Typography;
class App extends Component {
    state = {


        firstname: this.props.data.first_name,
        lastname: this.props.data.last_name,
        phonenumber: this.props.data.ph_no,
        email: this.props.data.email,
        degree: this.props.data.degree,
        oldpassword: "",
        newpassword: "",
        conformpassword: ""
    };




    onChangeInputBox1 = (e) => {
        this.setState({ firstname: e.target.value });

    };
    onChangeInputBox2 = (e) => {
        this.setState({ lastname: e.target.value });

    };
    onChangeInputBox3 = (e) => {
        this.setState({ phonenumber: e.target.value });

    };
    onChangeInputBox4 = (e) => {
        this.setState({ email: e.target.value });

    };
    onChangeInputBox5 = (e) => {
        this.setState({ degree: e.target.value });

    };
    onChangeInputBox6 = (e) => {
        this.setState({ oldpassword: e.target.value });

    };
    onChangeInputBox7 = (e) => {
        this.setState({ newpassword: e.target.value });

    };
    onChangeInputBox8 = (e) => {


        this.setState({ conformpassword: e.target.value });


    }

    submit = () => {
        let flag = 0;
        var pattern_phno = /^[6-9]\d{9}$/;
        var pattern_email = /\S+@\S+\.\S+/;
        var pattern_password = /^[A-Za-z]\w{6,14}$/;
        let firstname= this.state.firstname;
        let lastname= this.state.lastname;
        let phonenumber= this.state.phonenumber;
        let email= this.state.email;
        let degree= this.state.degree;
        let oldpassword= this.state.oldpassword;
        let newpassword=this.state.newpassword;
        let confirmpassword= this.state.conformpassword;

        if (!firstname === "" || !/^[A-Za-z ]+$/.test(firstname)) {
            message.error("Invaild Input Frist Name");
            flag = 1;
            return false;
        }
        else if (!lastname === "" || !/^[A-Za-z ]+$/.test(lastname)) {
            message.error("Invaild Input Last Name");
            flag = 1;
            return false;
        }
        else if (!phonenumber === "" || !pattern_phno.test(phonenumber)) {
            message.error("Invaild Input Phone Number");
            flag = 1;
            return false;
        }
        else if (!email === "" || !pattern_email.test(email)) {
            message.error("Invaild Input email");
            flag = 1;
            return false;
        }
        else if (!degree === "" || !/^[A-Za-z. ]+$/.test(degree)) {
            message.error("Invaild Input Degree");
            flag = 1;
            return false;
        }
        else if (!oldpassword === "" || !pattern_password.test(oldpassword)) {
            message.error("Invaild Input Password");
            flag = 1;
            return false;
        }
        else if (!newpassword === "" || !pattern_password.test(newpassword)) {
            message.error("Invaild Input Password");
            flag = 1;
            return false;
        }
        else if (newpassword !== confirmpassword) {
            message.error("Mismatch Password");
            flag = 1;
            return false;
        }



     
         else if(flag===0){
             let data = {
                 id: this.props.data._id,
                 firstname: this.state.firstname,
                 lastname: this.state.lastname,
                 phonenumber: this.state.phonenumber,
                 email: this.state.email,
                 degree: this.state.degree,
                 oldpassword: this.state.oldpassword,
                 newpassword: this.state.newpassword,
                 confirmpassword: this.state.conformpassword
             }
 
             axios.post(config.serverurl + "/pg_db/student/home/profile", data)
                 .then(res => {
                     if (res.data.Status === 1) {
                         message.info("Successfull Updated !");
                     }
                     else if (res.data.Status === -1) {
                         message.error("Old Password  Wrong !");
                     }
                     else {
                         message.error("Operation Failed !");
                     }
                 })
         }

    }

    render() {
    

        return (
            <div>

                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title>MY PROFILE</Title>

                    </Col>
                    <Col span={12} >
                        <Input placeholder="First Name" value={this.state.firstname} style={{ width: "100%" }} onChange={this.onChangeInputBox1} />

                    </Col>
                    <Col span={12}>
                        <Input placeholder="Last Name" value={this.state.lastname} style={{ width: "100%" }} onChange={this.onChangeInputBox2} />

                    </Col>
                    <Col span={12} >
                        <Input placeholder="Phone Number" value={this.state.phonenumber} style={{ width: "100%" }} onChange={this.onChangeInputBox3} />
                    </Col>
                    <Col span={12} >
                        <Input placeholder="Email" value={this.state.email} style={{ width: "100%" }} onChange={this.onChangeInputBox4} />

                    </Col>
                    <Col span={12} >
                        <Input placeholder="Degree" value={this.state.degree} style={{ width: "100%" }} onChange={this.onChangeInputBox5} />

                    </Col>
                    <Col span={12} >
                        <Input placeholder="Old Password" style={{ width: "100%" }} onChange={this.onChangeInputBox6} />

                    </Col>
                    <Col span={12} >
                        <Input placeholder="New Password" style={{ width: "100%" }} onChange={this.onChangeInputBox7} />

                    </Col>
                    <Col span={12} >
                        <Input placeholder="Conform Password" style={{ width: "100%" }} onChange={this.onChangeInputBox8} />
                    </Col>
                    <Col span={24} offset={9} >
                        <Button block type="primary" onClick={this.submit} style={{ width: "20%" }}>Submit</Button>
                    </Col>
                </Row>


            </div>
        );
    }
}


export default App;


