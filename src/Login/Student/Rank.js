import React, { Component } from "react";
import {
    Input, Row, Col, Button, message, Typography, Table, Tag, Form, Space
} from "antd";
import { SearchOutlined, DownloadOutlined } from '@ant-design/icons';
import axios from "axios";
import config from '../../config';
import Highlighter from 'react-highlight-words';
import { CSVLink } from "react-csv";
const { Title } = Typography;
class App extends Component {
    state = {
        status: 0,
        General: "",
        Bc: "",
        Mbc: "",
        Sc: "",
        searchText: '',
        searchedColumn: '',
    };

    componentDidMount() {
        axios.post(config.serverurl + "/pg_db/student/home/rank")
            .then(res => {
                this.setState({ status: 1, General: res.data.status })

            })
    }

    General = () => {
        axios.post(config.serverurl + "/pg_db/student/home/rank")
            .then(res => {
                this.setState({ status: 1, General: res.data.status })

            })

    }

    Bc = () => {
        axios.post(config.serverurl + "/pg_db/student/home/bc")
            .then(res => {
                this.setState({ status: 2, Bc: res.data.status })


            })
    }

    Mbc = () => {
        axios.post(config.serverurl + "/pg_db/student/home/mbc")
            .then(res => {
                this.setState({ status: 3, Mbc: res.data.status })


            })

    }
    Sc = () => {
        axios.post(config.serverurl + "/pg_db/student/home/sc")
            .then(res => {
                this.setState({ status: 4, Sc: res.data.status })


            })

    }


    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({ closeDropdown: false });
                            this.setState({
                                searchText: selectedKeys[0],
                                searchedColumn: dataIndex,
                            });
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    render() {


        const columns = [
            {
                title: 'Rank',
                key: '_id',
                render: (text, record) =>
                    this.state.status === 1 ? (this.state.General.indexOf(record) + 1)
                        : this.state.status === 2 ? (this.state.Bc.indexOf(record) + 1)
                            : this.state.status === 3 ? (this.state.Mbc.indexOf(record) + 1)
                                : this.state.status === 4 ? (this.state.Sc.indexOf(record) + 1)
                                    : "",
            },
            {
                title: 'User Id',
                key: '_id',
                dataIndex: '_id',
                render: (text, record) => (
                    <Tag color={this.props.data._id === record._id ? "green" : "default"}>
                        {record._id}
                    </Tag>
                ),
                ...this.getColumnSearchProps('_id'),


            },

            {
                title: 'Application Number',
                key: 'appliction_number',
                dataIndex: 'appliction_number',

                render: (text, record) => (
                    <Tag color={this.props.data._id === record._id ? "green" : "default"}>
                        {record.appliction_number}
                    </Tag>
                ),
                ...this.getColumnSearchProps('appliction_number'),

            },

            {
                title: 'Name',
                key: 'first_name',
                render: (text, record) => (
                    <Tag color={this.props.data._id === record._id ? "green" : "default"}>
                        {record.first_name + " " + record.last_name}
                    </Tag>
                ),
                //...this.getColumnSearchProps('first_name'),

            },
            {
                title: 'Mark',

                key: 'tancet_mark',
                render: (text, record) => (
                    <Tag color={this.props.data._id === record._id ? "green" : "default"}>
                        {record.tancet_mark}
                    </Tag>
                ),
                sorter: (a, b) => a.tancet_mark - b.tancet_mark

            },
            {
                title: 'Community',
                dataIndex: 'community',
                key: 'Community',


                filters: [
                    {
                        text: 'BC',
                        value: 'BC',
                    },
                    {
                        text: 'MBC',
                        value: 'MBC',
                    },
                    {
                        text: 'SC',
                        value: 'SC',
                    },
                    {
                        text: 'ST',
                        value: 'ST',
                    },



                ],
                onFilter: (value, record) => record.community.startsWith(value),
                filterSearch: true,

            }

        ]

        const headers = [
            { label: "User ID", key: "_id" },
            { label: "First Name", key: "first_name" },
            { label: "Last Name", key: "last_name" },
            { label: "Community", key: "community" },
            { label: "Tancet Mark", key: "tancet_mark" },
            { label: "Applicatoin number Name", key: "appliction_number" },

        ];
        return (
            <div>
                <Row gutter={[16, 24]}>
                    <Col span={24}>
                        <Title>Rank List</Title>

                    </Col>

                    <Col span={2}>
                        <Button block type="primary" onClick={this.General}>GENERAL</Button>
                    </Col>
                    <Col span={2}>
                        <Button block type="primary" onClick={this.Bc}>BC </Button>
                    </Col>
                    <Col span={2}>
                        <Button block type="primary" onClick={this.Mbc}>MBC</Button>
                    </Col>
                    <Col span={2}>
                        <Button block type="primary" onClick={this.Sc}>SC</Button>
                    </Col>

                    <Col span={24}>


                        {this.state.status === 1 ? <div>   <Tag color="green">Community: General</Tag></div>
                            : this.state.status === 2 ? (<div>  <Tag color="green">Community: Bc</Tag></div>)
                                : this.state.status === 3 ? (<div>   <Tag color="green">Community: Mbc</Tag></div>)
                                    : this.state.status === 4 ? (<div>   <Tag color="green">Community: Sc</Tag></div>)
                                        : ""
                        }
                    </Col>
                    <Col span={24}>
                        <Button type="primary">
                            <CSVLink
                                data={this.state.status === 1 ? this.state.General :
                                    this.state.status === 2 ? this.state.Bc :
                                        this.state.status === 3 ? this.state.Mbc :
                                            this.state.status === 4 ? this.state.Sc : ""}

                                headers={headers} filename={this.state.status === 1 ? "General Rank List" :
                                    this.state.status === 2 ? "BC Rank List" :
                                        this.state.status === 3 ? "MBC Rank List" :
                                            this.state.status === 4 ? "SC Rank List" : ""}>
                                <DownloadOutlined />Download Excel
                            </CSVLink></Button>
                    </Col>

                    <Col span={24}>
                        {this.state.status === 1 ? <div> <Table dataSource={this.state.General} columns={columns} /></div>
                            : this.state.status === 2 ? (<div> <Table dataSource={this.state.Bc} columns={columns} /></div>)
                                : this.state.status === 3 ? (<div> <Table dataSource={this.state.Mbc} columns={columns} /></div>)
                                    : this.state.status === 4 ? (<div> <Table dataSource={this.state.Sc} columns={columns} /></div>)
                                        : ""
                        }

                    </Col>





                </Row>

            </div>
        );
    }
}


export default App;


