import React, { Component } from "react";
import {
    Row, Col, Card, Button, Descriptions, Tag,Drawer
} from "antd";
import { CheckCircleTwoTone, FilePdfOutlined, MinusSquareTwoTone } from '@ant-design/icons';

import axios from "axios";
import config from '../../config';

class App extends Component {
    state = {
        status: "",
        uploadstatus: false,
        verifystatus: false,
        data: [],
        Visible:false,
        
    };
    componentDidMount() {
       
        let data = {
            _id: this.props.data._id,
        }

        axios.post(config.serverurl + "/pg_db/student/home/searchdata", data)
            .then(res => {
                this.setState({ data: res.data.user_details[0] });
                let status = res.data.user_details[0].status;
                let uploadstatus = true;
                let verifystatus = true;
                for (var key in status) {
                    if (parseInt(status[key]) < 0) uploadstatus = false;
                    if (parseInt(status[key]) !== 1) verifystatus = false;
                }
                this.setState({ uploadstatus: uploadstatus, verifystatus: verifystatus })
            })
    }

   

    render() {
       
        return (
            <div>
                <Row gutter={[16, 16]}>
                    <Col span={24}>
                        <Card
                            title="Applications"
                            
                            style={{ width: "100%" }}
                            bordered
                        >

                            <Card
                                title="Course name: MCA
                          "
                                style={{ width: "100%" }}
                                bordered
                            >
                                <Descriptions>
                                    <Descriptions.Item label="Application Number:">
                                        {this.props.data.appliction_number}
                                    </Descriptions.Item>
                                    <Descriptions.Item label="Name">
                                        {this.props.data.first_name} {this.props.data.last_name}
                                    </Descriptions.Item>
                                    <Descriptions.Item label="Community">
                                        {this.props.data.community}
                                    </Descriptions.Item>
                                    <Descriptions.Item label="Email">
                                        {this.props.data.email}
                                    </Descriptions.Item>
                                    <Descriptions.Item
                                        label="TANCET 2021 Registration Number"
                                    >
                                        {this.props.data.tancet_registration_number}
                                    </Descriptions.Item>
                                    <Descriptions.Item
                                        label="TANCET Marks"
                                    >
                                        25.575
                                    </Descriptions.Item>
                                    <Descriptions.Item
                                        label="Upload Status"
                                    >
                                        {this.state.uploadstatus ?
                                            <Tag color="green">Completed</Tag>
                                            : <Tag color="red">Pending</Tag>
                                        }
                                    </Descriptions.Item>

                                    <Descriptions.Item
                                        label="Certificate Verification Status"
                                    >
                                        {this.state.verifystatus ?
                                            <Tag color="green">Completed</Tag>
                                            : <Tag color="red">Pending</Tag>
                                        }
                                    </Descriptions.Item>
                                </Descriptions>
                            </Card>

                        </Card>
                    </Col>

                    

                </Row>

        

            </div>
        );
    }
}

export default App;