import React, { Component } from "react";
import {
  Card, Col, Row, Input, Button, Modal, PageHeader, message
} from "antd";
import { LogoutOutlined } from '@ant-design/icons';
import "./App.less";
import axios from "axios";
import Admin from './Login/Admin/index';
import Student from './Login/Student/index';
import Register from './Registation';
import config from './config';


class App extends Component {
  state = {
    USERNAME: "",
    PASSWORD: "",
    USERID: "",
    EMAIL: "",
    isModalVisible: false,
    isModalVisible1: false,
    status: "",
    userDetails: {},
    checked:false

  };


  onChangeInputBox1 = (e) => {
    this.setState({ USERNAME: e.target.value });

  };
  onChangeInputBox2 = (e) => {
    this.setState({ PASSWORD: e.target.value });

  };

  onChangeInputBox3 = (e) => {
    this.setState({ USERID: e.target.value });

  };

  onChangeInputBox4 = (e) => {
    this.setState({ EMAIL: e.target.value });

  };

  login = () => {
    let flag = 0;
    let USERNAME = this.state.USERNAME;
    let PASSWORD = this.state.PASSWORD;
    var pattern_username = /^[A-Za-z]\w{4,14}$/;
    var pattern_password = /^[A-Za-z]\w{4,14}$/;
    if (!USERNAME === "" || !pattern_username.test(USERNAME)) {
      message.error("Invaild Input Username");
      flag = 1;
      return false;
    }
    else if (!PASSWORD === "" || !pattern_password.test(PASSWORD)) {
      message.error("Invaild Password");
      flag = 1;
      return false;
    }
    else if (flag === 0) {
      let data = {
        USERNAME: this.state.USERNAME.trim(),
        PASSWORD: this.state.PASSWORD.trim(),
      }
      console.log(data);

      axios.post(config.serverurl + "/pg_db/login", data)
        .then(res => {
          if (res.data.Status === 1) {
            this.setState({ userDetails: res.data.user_details });
            if (res.data.user_details.role === 'student') this.setState({ status: 1 });
            else if (res.data.user_details.role === 'admin') this.setState({ status: 2 });
          }
          else {
            message.error("Invalid User !");
          }
        })
    }
  }




  showModal = () => {

    this.setState({ isModalVisible: true });

  }
  showModal1 = () => {

    this.setState({ isModalVisible1: true });

  }

  handleOk = () => {
    let data = {
      USERID: this.state.USERID.trim(),
      EMAIL: this.state.EMAIL.trim(),
    }
    axios.post(config.serverurl + "/pg_db/login/forgotpassword", data)
      .then(res => {
        if (res.data.Status === 1) {
          message.success("sucessfully sent");
        }
        else if (res.data.Status === 0) {
          message.error("enter correct email id");
        }


      })


    this.setState({ isModalVisible: false });

  }

  handleOk1 = () => {
    this.setState({ isModalVisible1: false });

  }

  handleCancel = () => {
    this.setState({ isModalVisible: false });
  }

  handleCancel1 = () => {
    this.setState({ isModalVisible1: false });
  }

  logout = () => {
    this.setState({ status: 0 });
    window.location.reload();
  }


  componentDidMount() {
    axios.post(config.serverurl + "/pg_db/login/checked")
      .then(async res => {
        this.setState({ checked: res.data.checked })
      })


  }


  render() {
console.log(this.state.checked)

    return (
      <div>
        <PageHeader
          className="site-page-header"
          title="TAMIL NADU MBA / MCA ONLINE COUNSELLING ADMISSION - 2021"
          subTitle="GOVERNMENT OF TAMIL NADU"
          extra={[
            <Button type="primary" key="1" onClick={this.logout} danger><LogoutOutlined />Logout</Button>,
          ]}
        />

        {this.state.status === 1 ? <Student data={this.state.userDetails} />
          : this.state.status === 2 ? (<Admin data={this.state.userDetails} />) : (
            <div>

              <Row gutter={24}>
                <Col span={6} offset={10}>
                  <Card title="Login" style={{ width: 300, marginTop: 300 }} bordered >
                    <Row gutter={[16, 16]} align="middle">
                      <Col span={24}><Input placeholder="Username" onChange={this.onChangeInputBox1} /></Col>
                      <Col span={24}><Input placeholder="Password" onChange={this.onChangeInputBox2} /></Col>
                      <Col span={24}><Button block type="primary" onClick={this.login}>Submit</Button></Col>
                      <Col span={24} >  <Button block type="link" onClick={this.showModal}>Forgot Password</Button></Col>
                      <Col span={24} >  <Button block type="link" onClick={this.showModal1} disabled={this.state.checked===false?true:false}>Register</Button></Col>
                    </Row>
                  </Card>
                </Col>
              </Row>


              <Modal title="Forgot Password" visible={this.state.isModalVisible} onOk={this.handleOk} onCancel={this.handleCancel}>
                <Row gutter={[16, 16]} align="middle">
                  <Col span={12}><Input placeholder="User Id" onChange={this.onChangeInputBox3} /></Col>
                  <Col span={12}><Input placeholder="Email" onChange={this.onChangeInputBox4} /></Col>
                </Row>
              </Modal>

              <Modal title="Register" visible={this.state.isModalVisible1} footer={null} onCancel={this.handleCancel1} destroyOnClose>
                <Register />
              </Modal>




            </div>


          )}

      </div>
    );
  }
}


export default App;


